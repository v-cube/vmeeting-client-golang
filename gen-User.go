package vmeeting

// This is file is auto-generated.  Do not modify.  See ./gen/VIDClient-gen.go for more clues

import (
	"encoding/json"
	// "fmt"
)

func (v *VMeetingClient) Login(id, pw, lang, country, location string) (*VMeetingLoginResponse, error) {
	params := map[string]interface{}{
		"action_login": "",
		"id":           id,
		"pw":           pw,
		"lang":         lang,
		"country":      country,
		"location":     location,
		"service":      "messenger",
		"output_type":  "json",
		"enc":          "sha1",
	}

	resp, err := v.fRest("/user", "POST", params)

	vid_response := &VMeetingLoginResponse{}

	err = json.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

func (v *VMeetingClient) LoginVcubeId(id, pw, lang, country, location string) (*VMeetingLoginVcubeIdResponse, error) {
	params := map[string]interface{}{
		"action_vcubeid_login": "",
		// "vcubeid_token": auth_token,
		"lang":        lang,
		"country":     country,
		"location":    location,
		"service":     "messenger",
		"output_type": "json",
		// timezone 				: timezone,
	}

	resp, err := v.fRest("/user", "POST", params)

	vid_response := &VMeetingLoginVcubeIdResponse{}

	err = json.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

func (v *VMeetingClient) AddRoomOneTimeMeeting(n2my_session string) (*VMeetingAddRoomOneTimeMeetingResponse, error) {
	params := map[string]interface{}{
		"action_add_room_one_time_meeting": "",
		"n2my_session":                     n2my_session,
		"output_type":                      "json",
	}

	resp, err := v.fRest("/user/meeting", "POST", params)

	vid_response := &VMeetingAddRoomOneTimeMeetingResponse{}

	err = json.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

func (v *VMeetingClient) CreateMeeting(n2my_session, room_id string) (*VMeetingCreateMeetingResponse, error) {
	params := map[string]interface{}{
		"action_create": "",
		"n2my_session":  n2my_session,
		"room_id":       room_id,
		"output_type":   "json",
	}

	resp, err := v.fRest("/user/meeting", "POST", params)

	vid_response := &VMeetingCreateMeetingResponse{}

	err = json.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

func (v *VMeetingClient) GetInviteUrl(n2my_session, meeting_id string) (*VMeetingGetInviteUrlResponse, error) {
	params := map[string]interface{}{
		"action_get_invite_url": "",
		"n2my_session":          n2my_session,
		"meeting_id":            meeting_id,
		"output_type":           "json",
	}

	resp, err := v.fRest("/user/meeting", "POST", params)

	vid_response := &VMeetingGetInviteUrlResponse{}

	err = json.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

func (v *VMeetingClient) GetVcubeMeetingRooms(n2my_session, meeting_id string) (*VMeetingGetVcubeMeetingRoomsResponse, error) {
	params := map[string]interface{}{
		"action_get_room_list": "",
		"n2my_session":         n2my_session,
		"output_type":          "json",
	}

	resp, err := v.fRest("/user", "POST", params)

	vid_response := &VMeetingGetVcubeMeetingRoomsResponse{}

	err = json.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}

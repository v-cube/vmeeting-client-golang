// +build ignore
package main

//go:generate go run VMeetingClient-gen.go

import (
	"fmt"
	"log"
	"os"
)

var commands = []Command{
	{file: "User", method: "Login", params_sig: "id, pw, lang, country, location string", params: `map[string]interface{}{
		"action_login": "",
		"id": id,
		"pw": pw,
		"lang": lang,
		"country": country,
		"location": location,
		"service": "messenger",
		"output_type": "json",
		"enc": "sha1",
	}`, returnType: "VMeetingLoginResponse", restPath: "/user", httpMethod: "POST"},
	{file: "User", method: "LoginVcubeId", params_sig: "id, pw, lang, country, location string", params: `map[string]interface{}{
		"action_vcubeid_login": "",
		// "vcubeid_token": auth_token,
		"lang": lang,
		"country": country,
		"location": location,
		"service": "messenger",
		"output_type": "json",
		// timezone 				: timezone,
	}`, returnType: "VMeetingLoginVcubeIdResponse", restPath: "/user", httpMethod: "POST"},
	{file: "User", method: "AddRoomOneTimeMeeting", params_sig: "n2my_session string", params: `map[string]interface{}{
		"action_add_room_one_time_meeting": "",
		"n2my_session": n2my_session,
		"output_type": "json",
	}`, returnType: "VMeetingAddRoomOneTimeMeetingResponse", restPath: "/user/meeting", httpMethod: "POST"},
	{file: "User", method: "CreateMeeting", params_sig: "n2my_session, room_id string", params: `map[string]interface{}{
		"action_create": "",
		"n2my_session": n2my_session,
		"room_id": room_id,
		"output_type": "json",
	}`, returnType: "VMeetingCreateMeetingResponse", restPath: "/user/meeting", httpMethod: "POST"},
	{file: "User", method: "GetInviteUrl", params_sig: "n2my_session, meeting_id string", params: `map[string]interface{}{
		"action_get_invite_url": "",
		"n2my_session": n2my_session,
		"meeting_id": meeting_id,
		"output_type": "json",
	}`, returnType: "VMeetingGetInviteUrlResponse", restPath: "/user/meeting", httpMethod: "POST"},
	{file: "User", method: "GetVcubeMeetingRooms", params_sig: "n2my_session, meeting_id string", params: `map[string]interface{}{
		"action_get_room_list": "",
		"n2my_session": n2my_session,
		"output_type": "json",
	}`, returnType: "VMeetingGetVcubeMeetingRoomsResponse", restPath: "/user", httpMethod: "POST"},
}

type Command struct {
	file, method, params_sig, params, returnType, restPath, httpMethod string
}

func AppendFile(filename, data string) {
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}
	defer file.Close()

	len, err := file.WriteString(data)
	if err != nil {
		log.Fatalf("failed writing to file: %s", err)
	}
	fmt.Printf("\nLength: %d bytes", len)
	fmt.Printf("\nFile Name: %s\n", file.Name())
}

func main() {
	method_counts := make(map[string]int)

	for _, command := range commands {
		file := "../gen-" + command.file + ".go"

		_, err := os.Stat(file)

		if err == nil {
			os.Remove(file)
		}
	}

	for _, command := range commands {
		file := "../gen-" + command.file + ".go"

		_, err := os.Stat(file)

		if err != nil {

			fmt.Println("err", err)
		}

		package_declaration := ""

		if method_counts[command.file] == 0 {
			package_declaration = `
package vmeeting

// This is file is auto-generated.  Do not modify.  See ./gen/VIDClient-gen.go for more clues

import (
	"encoding/json"
	// "fmt"
)
`
		}

		data := fmt.Sprintf(`
%s

func (v *VMeetingClient) %s(%s) (*%s, error) {
	params := %s

	resp,err := v.fRest("%s", "%s", params)

	vid_response := &%s{}

	err = json.Unmarshal(resp.Body(), vid_response)

	// fmt.Println("vid_response:", vid_response)

	if err != nil {
		return nil, err
	}

	return vid_response, err
}
		`, package_declaration, command.method, command.params_sig, command.returnType, command.params, command.restPath, command.httpMethod, command.returnType)

		AppendFile("../gen-"+command.file+".go", data)

		fmt.Println("method_counts[command.file]", method_counts[command.file])

		method_counts[command.file] = method_counts[command.file] + 1
	}
}

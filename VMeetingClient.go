package vmeeting

import (
	"fmt"
	"net/url"

	"github.com/go-resty/resty/v2"
)

type VMeetingClient struct {
	config *VMeetingClientConfig
}

type VMeetingClientConfig struct {
	BaseUrl string
}

type VMeetingResponse struct {
	Status    int    `json:"status"`
	ErrorMsg  string `json:"error_msg"`
	ErrorInfo struct {
		ErrorCode string `json:"error_cd"`
		Data      string `json:"data"`
		Method    string `json:"method"`
	} `json:"error_info"`
}

type VMeetingUser struct {
	Type                  string           `json:"type"`
	Session               string           `json:"session"`
	Save_sharing_file_flg int              `json:"save_sharing_file_flg"`
	Service_type          string           `json:"service_type"`
	Enable_super_visor    string           `json:"enable_super_visor"`
	UserInfo              VMeetingUserInfo `json:"user_info"`
}

type VMeetingUserInfo struct {
	UserId   string `json:"user_id"`
	UserName string `json:"user_name"`
}

type VMeetingRoom struct {
	RoomId   string `json:"room_id"`
	MaxSeat  int    `json:"max_seat"`
	RoomName string `json:"room_name"`
}

type VMeetingRoomInfo struct {
	MeetingId    string `json:"meeting_id"`
	MeetingName  string `json:"meeting_name"`
	PasswordFlag string `json:"password_flg"`
	PinCode      string `json:"pin_cd"`
}

type VMeetingInviteInfo struct {
	MeetingId     string `json:"meeting_id"`
	PinCode       string `json:"pin_cd"`
	UserInviteUrl string `json:"user_invite_url"`
}

type VMeetingLoginResponse struct {
	VMeetingResponse
	User VMeetingUser `json:"data"`
}

type VMeetingLoginVcubeIdResponse struct {
	VMeetingResponse
	User VMeetingUser `json:"data"`
}

type VMeetingAddRoomOneTimeMeetingResponse struct {
	VMeetingResponse
	Room VMeetingRoom `json:"data"`
}

type VMeetingCreateMeetingResponse struct {
	VMeetingResponse
	RoomInfo VMeetingRoomInfo `json:"data"`
}

type VMeetingGetInviteUrlResponse struct {
	VMeetingResponse
	InviteInfo VMeetingInviteInfo `json:"data"`
}

type VMeetingGetVcubeMeetingRoomsResponse struct {
	VMeetingResponse
	RoomsInfo struct {
		Rooms struct {
			RoomInfo []struct {
				RoomInfo VMeetingRoom `json:"room_info"`
			} `json:"room"`
		} `json:"rooms"`
	} `json:"data"`
}

func (v *VMeetingClient) do_http_post(form_data map[string]interface{}) (*VMeetingResponse, error) {
	return nil, nil
}

func (v *VMeetingClient) fRest(rest_path, method string, params map[string]interface{}) (*resty.Response, error) {
	// fmt.Println("------ VIDClient.go:: func (v *VIDClient) fRest(rest_path, method string, params map[string]string) (*VIDResponse, error) {")

	_params := ""

	for key, value := range params {
		_value := fmt.Sprintf("%v", value)

		_params = _params + key + "=" + url.QueryEscape(_value) + "&"
	}

	// fmt.Println("_params:", _params)
	// fmt.Println("params:", params)

	config := v.config

	base_url := config.BaseUrl

	// fmt.Println("base_url:", base_url)

	// restDetail := &RestDetail{
	// 	created:  fmt.Sprint(time.Now().Format(time.RFC3339)),
	// 	password: config.RestPassword,
	// 	username: config.ConsumerKey,
	// }

	// if config.NonceFn == nil {
	// 	restDetail.nonce = DefaultNonceFn()
	// } else {
	// 	restDetail.nonce = config.NonceFn()
	// }

	// restDetail.nonce = fmt.Sprintf("%x", sha1.Sum([]byte(restDetail.nonce)))
	// restDetail.nonce_64 = base64.StdEncoding.EncodeToString([]byte((restDetail.nonce)))

	// restDetail.passwordDigest = base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%x", sha1.Sum([]byte(restDetail.nonce+restDetail.created+restDetail.password)))))

	// fmt.Println("restDetail:", *restDetail)

	headers := map[string]string{
		// "Content-Type": "application/json",
		// "X-WSSE":        fmt.Sprintf("UsernameToken Username=\"%s\", PasswordDigest=\"%s\", Nonce=\"%s\", Created=\"%s\"", restDetail.username, restDetail.passwordDigest, restDetail.nonce_64, restDetail.created),
		"Cache-Control": "no-cache",
	}

	// Create a Resty Client
	client := resty.New()

	var fn func(string) (*resty.Response, error)

	// test
	// fmt.Println("1111 fn:", fn)

	request := client.R().SetHeaders(headers)

	// fmt.Println("request:", request)

	switch method {
	case "GET":
		fn = request.SetQueryString(_params).EnableTrace().Get
	case "POST":
		fn = request.SetQueryString(_params).SetHeader("Content-Type", "text/plain").EnableTrace().Post
	}

	// fmt.Println("2222 fn:", fn)
	// fmt.Println("2222 request:", request)

	// fmt.Printf("Connecting to %v... \n", base_url+rest_path)

	resp, err := fn(base_url + rest_path)

	// fmt.Println("resp.RawBody():", resp.String())

	// Explore response object
	// fmt.Println("Response Info:")
	// fmt.Println("Error      :", err)
	// fmt.Println("Header		:", resp.Header())
	// fmt.Println("Status Code:", resp.StatusCode())
	// fmt.Println("Status     :", resp.Status())
	// fmt.Println("Time       :", resp.Time())
	// fmt.Println("Received At:", resp.ReceivedAt())
	// fmt.Println("Body       :\n", resp)
	// fmt.Println()

	// Explore trace info
	// fmt.Println("Request Trace Info:")
	// ti := resp.Request.TraceInfo()
	// fmt.Println("DNSLookup    :", ti.DNSLookup)
	// fmt.Println("ConnTime     :", ti.ConnTime)
	// fmt.Println("TLSHandshake :", ti.TLSHandshake)
	// fmt.Println("ServerTime   :", ti.ServerTime)
	// fmt.Println("ResponseTime :", ti.ResponseTime)
	// fmt.Println("TotalTime    :", ti.TotalTime)
	// fmt.Println("IsConnReused :", ti.IsConnReused)
	// fmt.Println("IsConnWasIdle:", ti.IsConnWasIdle)
	// fmt.Println("ConnIdleTime :", ti.ConnIdleTime)

	if err != nil {
		return nil, err
	}

	return resp, err
}

func NewVMeetingClient(config *VMeetingClientConfig) *VMeetingClient {
	return &VMeetingClient{
		config: config,
	}
}

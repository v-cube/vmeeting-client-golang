package vmeeting

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func TestNewClient(t *testing.T) {
	jsonFile, err := os.Open("config_dev.json")
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var dev_config = make(map[string]string)

	json.Unmarshal(byteValue, &dev_config)

	fmt.Println("dev_config:", dev_config)

	defer jsonFile.Close()

	config := &VMeetingClientConfig{
		// ConsumerKey:    dev_config["consumer_key"],
		// RestPassword:   dev_config["rest_password"],
		BaseUrl: dev_config["rest_url"],
		// SecretAuthCode: dev_config["secret_auth_code"],
	}

	vmeeting_user1 := dev_config["vmeeting_user1"]
	vmeeting_user1_password := dev_config["vmeeting_user1_password"]

	client := NewVMeetingClient(config)

	fmt.Println("client:", client)

	login_resp, err := client.Login(vmeeting_user1, vmeeting_user1_password, "en", "auto", "138.75.230.115")

	if err != nil {
		panic(err)
	}

	fmt.Println("login_resp:", login_resp)
	fmt.Println("login_resp.User:", login_resp.User)

	n2my_session := login_resp.User.Session

	if n2my_session != "" {
		getrooms_resp, err := client.GetVcubeMeetingRooms(n2my_session, vmeeting_user1)

		if err != nil {
			panic(err)
		}

		fmt.Println("getrooms_resp:", getrooms_resp)

		add_room_resp, err := client.AddRoomOneTimeMeeting(n2my_session)

		if err != nil {
			panic(err)
		}

		fmt.Println("add_room_resp:", add_room_resp)

		create_resp, err := client.CreateMeeting(n2my_session, add_room_resp.Room.RoomId)

		if err != nil {
			panic(err)
		}

		fmt.Println("create_resp:", create_resp)

		get_invite_resp, err := client.GetInviteUrl(n2my_session, create_resp.RoomInfo.MeetingId)

		if err != nil {
			panic(err)
		}

		fmt.Println("get_invite_resp:", get_invite_resp)
	}
}
